import {dayOneInput} from './inputs/day_1'

export default function() 
{
    const directions = dayOneInput.split("");
    const floor = new Array; 
 
    for (let i = 0; i <= directions.length; i++)
    {
        if (directions[i] === '(') 
        {
            floor.push(1);
        }
        else if (directions[i] === ')') 
        {   
            floor.push(-1);
        }
    }

    let finalFloor = Number();
    const basementVisits = new Array;  
    
    for (let i = 0; i < floor.length; i++)
    {   
        finalFloor += floor[i];
        if (finalFloor == -1) 
        {
            basementVisits.push(i + 1);
        }
    }
    
    console.log(`Day One: The instructions take Santa to floor: ${finalFloor}. He first enters the basement on the ${basementVisits[0]}th instruction.`);
}
