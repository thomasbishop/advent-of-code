import dayOne2015 from './2015/day_1';
//import dayOne2015Reworked from './2015/day_1__reworked';
import dayTwo2015 from './2015/day_2';
import chalk from 'chalk'; 

console.log(chalk.blue.bold('>>> 2015 Solutions >>> '));

dayOne2015();
//dayOne2015Reworked();

dayTwo2015();
